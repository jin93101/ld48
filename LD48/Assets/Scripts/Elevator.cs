using UnityEngine;
using UnityEngine.SceneManagement;

public class Elevator : MonoBehaviour
{
    [SerializeField] float _speed;
    private float baseSpeed = 3f;
    private Animator anim;

    //enemy activation
    public bool enemyActivated= false;
    public bool enemyLevel01Activated = false;
    public bool enemyLastLevelActivated = false;

    ElevatorButton elevatorButton;
    Player player;

    public bool canGoUp = false;
    public int floorCounter = 0;

    private void Start()
    {
        elevatorButton = FindObjectOfType<ElevatorButton>();
        player = FindObjectOfType<Player>();
        _speed = baseSpeed;
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        //moving elevator
        if(elevatorButton.elevatorIsMoving == true && canGoUp == false)
        {
            //sound here
            _speed = baseSpeed;
            anim.SetBool("isRunning", true);
            transform.Translate(Vector2.down * _speed * Time.deltaTime);
        }

        if(player.missingWheel == 3)
        {
            canGoUp = true;
        }

        //This is the final ride with elevator to the top of the screen where will be the Happy end
        if (elevatorButton.elevatorIsMoving == true && canGoUp == true)
        {
            _speed = baseSpeed;
            anim.SetBool("isRunning", true);
            transform.Translate(Vector2.up * _speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("LevelMark") && canGoUp == false)
        {
            //elevator stops
            //sound here
            floorCounter = 1;
            ActivateEnemy();
            elevatorButton.elevatorIsMoving = false;
            _speed = 0f;
            anim.SetBool("isRunning", false);
        }else if(collision.CompareTag("LastLevelMark") && canGoUp == false)
        {
            floorCounter = 2;
            ActivateLastLevelEnemy();
            elevatorButton.elevatorIsMoving = false;
            _speed = 0f;
            anim.SetBool("isRunning", false);
        }else if (collision.CompareTag("WinMark"))
        {
            SceneManager.LoadScene("05WinScene");
        }
    }

    public void ActivateEnemy()
    {
        enemyActivated = true;
    }

    public void ActivateLevel01Enemy()
    {
        enemyLevel01Activated = true;
    }

    public void ActivateLastLevelEnemy()
    {
        enemyLastLevelActivated = true;
    }
}
