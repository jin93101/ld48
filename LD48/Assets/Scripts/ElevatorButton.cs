using UnityEngine;
using UnityEngine.UI;

public class ElevatorButton : MonoBehaviour
{
    private bool goDownButton = false;
    public bool elevatorIsMoving = false;

    public GameObject TextE;
    public Text pressE;
    public GameObject fixElevatorE;
    public Text fixElevator;

    public GameObject pressSound;

    Elevator elevator; // I need floorCounter from elevator
    Player player; // I need missingWheel from player

    private void Start()
    {
        elevator = FindObjectOfType<Elevator>();
        player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        pressE.text = "[E] Go Deeper";
        fixElevator.text = "[E] Fix Elevator!";

        if (goDownButton == true && Input.GetKeyDown(KeyCode.E))
        {
            Instantiate(pressSound, transform.position, Quaternion.identity);
            elevatorIsMoving = true;
            if(elevatorIsMoving == true)
            {
                TextE.gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if player is close to the button, it can be pressed
        if (collision.CompareTag("Player") && player.missingWheel == 1 && elevator.floorCounter == 0 && elevatorIsMoving == false)
        {
            TextE.gameObject.SetActive(true);
            goDownButton = true;
        } 
        else if(collision.CompareTag("Player") && player.missingWheel == 2 && elevator.floorCounter == 1 && elevatorIsMoving == false)
        {
            TextE.gameObject.SetActive(true);
            goDownButton = true;
        }
        else if (collision.CompareTag("Player") && player.missingWheel == 3 && elevator.floorCounter == 2 && elevatorIsMoving == false)
        {
            fixElevatorE.gameObject.SetActive(true);
            goDownButton = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            TextE.gameObject.SetActive(false);
            goDownButton = false;
        }
    }
}
