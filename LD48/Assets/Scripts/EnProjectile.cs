using UnityEngine;

public class EnProjectile : MonoBehaviour
{
    //to do: destroy projectli once it hits walls (once I have the walls)

    public float speed;
    public Rigidbody2D rb;
    public GameObject damageEffect;

    private float selfDestroyAfter = 3f; 

    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    private void Update()
    {
        selfDestroyAfter -= Time.deltaTime;
        if(selfDestroyAfter <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy") || collision.CompareTag("Wall"))
        {
            Instantiate(damageEffect, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
