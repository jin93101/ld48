using UnityEngine;

public class EnemyLastFloor : MonoBehaviour
{
    public float speed;
    private float baseSpeed = 2f;
    public int damage = 2;

    public int health = 20;

    public bool isFlipped = false;

    public GameObject projectile;
    private Transform player;
    private Rigidbody2D rb;
    public GameObject deathSound;
    public GameObject deathAnimation;

    public GameObject spillGreenBlood;

    Elevator elevator;
    Granade granade;

    void Start()
    {
        baseSpeed = Random.Range(2f, 4f);

        rb = GetComponent<Rigidbody2D>();
        elevator = FindObjectOfType<Elevator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        LookAtPlayer();

        //activates Zombies on the first Floor
        if (elevator.enemyLastLevelActivated == true)
        {
            speed = baseSpeed;
        }
        else
        {
            speed = 0f;
        }

        Vector2 target = new Vector2(player.position.x, rb.position.y);
        Vector2 newPos = Vector2.MoveTowards(rb.position, target, speed * Time.fixedDeltaTime);
        rb.MovePosition(newPos);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Projectile"))
        {
            TakeDamage(damage);
        }

        if (collision.CompareTag("Granade"))
        {
            FindObjectOfType<Granade>().Explode();
            TakeDamage(damage * 10);
        }
    }

    public void TakeDamage(int damage)
    {
        Instantiate(spillGreenBlood, transform.position, Quaternion.identity);
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Instantiate(deathSound, transform.position, Quaternion.identity);
        Instantiate(deathAnimation, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    //enemy should alway face the player
    public void LookAtPlayer()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if (transform.position.x < player.position.x && isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = false;
        }
        else if (transform.position.x > player.position.x && !isFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            isFlipped = true;
        }
    }
}
