using UnityEngine;

public class Granade : MonoBehaviour
{
    private Rigidbody2D rb;
    private float countdown;
    private float baseCountdown = 3f;

    public GameObject explosion;
    public GameObject explosionSound;

    private void Start()
    {
        countdown = baseCountdown;
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-3f, 10f);
    }

    private void Update()
    {
        if(countdown <= 0)
        {
            Explode();
            countdown = baseCountdown;
        }else
        {
            countdown -= Time.deltaTime;
        }
    }

    public void Explode()
    {
        //effect
        Instantiate(explosionSound, transform.position, Quaternion.identity);
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
