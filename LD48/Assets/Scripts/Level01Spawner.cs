using UnityEngine;

public class Level01Spawner : MonoBehaviour
{
    public GameObject[] spawnCollection;
    public float timer = 5f;
    public float startTimer;

    public float minRange = 2f;
    public float maxRange = 5f;

    private void Update()
    {
        startTimer = Random.Range(minRange, maxRange);

        if(timer > 0)
        {
            timer -= Time.deltaTime;
        }else
        {
            int rand = Random.Range(0, spawnCollection.Length);
            Instantiate(spawnCollection[rand], transform.position, Quaternion.identity);
            timer = startTimer;
        }
    }
}
