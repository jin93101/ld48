using UnityEngine;

public class MissingWheel : MonoBehaviour
{
    public GameObject pickUpSound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Instantiate(pickUpSound, transform.position, Quaternion.identity);
            //Instantiate some effect
            //and play some sound
            Destroy(gameObject);
        }

    }
}
