using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    [SerializeField] float runSpeed = 2f;
    private Rigidbody2D rb;
    private float horizontalMove;

    public int health = 3;
    public int damage = 1;

    private Animator anim;

    //ground check for jumping
    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;
    public float jumpForce;
    private int extraJumps;
    public int extraJumpsValue;

    //flipping the character to correct side
    private bool faceRight = true;

    Elevator elevator;
    SceneChanger sceneChanger;
    public Text numberOfLifes;
    public Text missingWheelsCollected;

    public int missingWheel = 0;

    public GameObject dropBlood;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        elevator = FindObjectOfType<Elevator>();
        sceneChanger = FindObjectOfType<SceneChanger>();
    }

    private void Update()
    {

        numberOfLifes.text = "Lifes: " + health.ToString();
        missingWheelsCollected.text = "Collected " + missingWheel + "/3 wheels";

        horizontalMove = Input.GetAxis("Horizontal");

        if (isGrounded == true)
        {
            extraJumps = extraJumpsValue;
        }

            if (Input.GetKeyDown(KeyCode.Space) && extraJumps > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
        }else if(Input.GetKeyDown(KeyCode.Space) && extraJumps == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
        }
        
        if(horizontalMove == 0)
        {
            anim.SetBool("isRunning", false);
        }
        else
        {
            anim.SetBool("isRunning", true);
        }
        
    }

    private void FixedUpdate()
    {

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        Move();
        if(faceRight == false && horizontalMove > 0)
        {
            Flip();
        }else if(faceRight == true && horizontalMove < 0)
        {
            Flip();
        }
    }

    private void Move()
    {
        float x = horizontalMove;
        float moveBy = x * runSpeed;
        rb.velocity = new Vector2(moveBy, rb.velocity.y);
    }

    private void Flip()
    {
        faceRight = !faceRight;
        transform.Rotate(0f, 180f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("EnemyHands"))
        {
            //sound here
            Instantiate(dropBlood, transform.position, Quaternion.identity);
            TakeDamage(damage);
        }

        if (collision.CompareTag("LevelMarkFirstFloor"))
        {
            elevator.ActivateLevel01Enemy();
        }

        if (collision.CompareTag("MissingWheel"))
        {
            missingWheel++;
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Destroy(gameObject);
        sceneChanger.GameOveScreen();
    }

}
