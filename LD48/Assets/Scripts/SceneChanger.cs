using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    
    public void ChangeSceneToMain()
    {
        SceneManager.LoadScene("01MainScreen");
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("03GameScene02");
    }

    public void GameOveScreen()
    {
        SceneManager.LoadScene("04GameOverScene");
    }
}
