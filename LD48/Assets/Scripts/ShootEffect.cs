using UnityEngine;

public class ShootEffect : MonoBehaviour
{
    private float timeToDestroy = 2f;

    void Update()
    {
        timeToDestroy -= Time.deltaTime;
        if(timeToDestroy <= 0)
        {
            Destroy(gameObject);
        }
    }
}
