using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject projectile;
    public GameObject granade;
    public Transform shotPoint;
    public GameObject shotSound;

    public float timer;
    private float resetTimer = 2f;

    private bool readyToThrow = true;

    private void Start()
    {
        timer = resetTimer;
    }

    void Update()
    {
        
            if (Input.GetKeyDown(KeyCode.X))
            {
            Instantiate(shotSound, transform.position, Quaternion.identity);
                Shoot();   
            }

        if(readyToThrow == true)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                Granade();
                readyToThrow = false;
            }
        }

        
        if (readyToThrow == false)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                readyToThrow = true;
                timer = resetTimer;
            }
        }  
    }

    private void Shoot()
    {
        Instantiate(projectile, shotPoint.position, shotPoint.rotation);
    }

    private void Granade()
    {
        Instantiate(granade, shotPoint.position, shotPoint.rotation);
    }
}
